package org.gameoflife

import com.gameoflife.GameOfLife
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GameOfLifeRulesTest {
  @Test
  // Any live cell with fewer than two live neighbours dies, as if by underpopulation.
  fun underpopulationTest() {
    val gameOfLife = GameOfLife(
      arrayOf(
        intArrayOf(1, 0, 0),
        intArrayOf(0, 1, 0),
        intArrayOf(0, 0, 0)
      )
    )

    val transformedMatrix = arrayOf(
      intArrayOf(0, 0, 0),
      intArrayOf(0, 0, 0),
      intArrayOf(0, 0, 0)
    )
    gameOfLife.transform()
    assertArrayEquals(transformedMatrix, gameOfLife.grid)
  }


  @Test
  // Any live cell with two or three live neighbours lives on to the next generation.
  fun nextGenerationTest() {
    val gameOfLife = GameOfLife(
      arrayOf(
        intArrayOf(0, 0, 0, 0),
        intArrayOf(0, 1, 1, 0),
        intArrayOf(0, 1, 1, 0),
        intArrayOf(0, 0, 0, 0)
      )
    )

    val transformedMatrix = arrayOf(
      intArrayOf(0, 0, 0, 0),
      intArrayOf(0, 1, 1, 0),
      intArrayOf(0, 1, 1, 0),
      intArrayOf(0, 0, 0, 0)
    )
    gameOfLife.transform()
    assertArrayEquals(transformedMatrix, gameOfLife.grid)
  }

  @Test
  // Any live cell with more than three live neighbours dies, as if by overpopulation.
  fun overpopulationTest() {
    val gameOfLife = GameOfLife(
      arrayOf(
        intArrayOf(1, 0, 1),
        intArrayOf(0, 1, 0),
        intArrayOf(1, 0, 1)
      )
    )

    val transformedMatrix = arrayOf(
      intArrayOf(0, 0, 0),
      intArrayOf(0, 0, 0),
      intArrayOf(0, 0, 0)
    )
    gameOfLife.transform()
    assertArrayEquals(transformedMatrix, gameOfLife.grid)
  }

  @Test
  //Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
  fun reproductionTest() {
    val gameOfLife = GameOfLife(
      arrayOf(
        intArrayOf(1, 0, 1),
        intArrayOf(0, 0, 0),
        intArrayOf(1, 0, 0)
      )
    )

    val transformedMatrix = arrayOf(
      intArrayOf(1, 1, 1),
      intArrayOf(1, 1, 1),
      intArrayOf(1, 1, 1)
    )
    gameOfLife.transform()
    assertArrayEquals(transformedMatrix, gameOfLife.grid)
  }

  @Test
  fun applyRules() {
    val gameOfLife3X3 = GameOfLife(
      arrayOf(
        intArrayOf(1, 0, 0),
        intArrayOf(0, 1, 0),
        intArrayOf(0, 0, 0)
      )
    )

    assertEquals(gameOfLife3X3.applyRules(1, 1), 0)
    assertEquals(gameOfLife3X3.applyRules(1, 2), 1)
    assertEquals(gameOfLife3X3.applyRules(1, 3), 1)
    assertEquals(gameOfLife3X3.applyRules(1, 4), 0)
    assertEquals(gameOfLife3X3.applyRules(0, 3), 1)
  }
}
