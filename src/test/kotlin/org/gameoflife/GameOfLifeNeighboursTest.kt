package org.gameoflife

import com.gameoflife.GameOfLife
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GameOfLifeNeighboursTest {

  private val gameOfLife3X3 = GameOfLife(
    arrayOf(
      intArrayOf(1, 0, 0),
      intArrayOf(0, 1, 0),
      intArrayOf(0, 0, 0)
    )
  )

  private val gameOfLife5X4 = GameOfLife(
    arrayOf(
      intArrayOf(1, 1, 0, 0, 1),
      intArrayOf(1, 1, 0, 0, 1),
      intArrayOf(0, 0, 0, 0, 0),
      intArrayOf(0, 1, 0, 0, 1)
    )
  )

  @Test
  fun getNeighboursCordsCenterCords() {
    val expected = listOf(
      intArrayOf(0, 0),
      intArrayOf(0, 1),
      intArrayOf(0, 2),
      intArrayOf(1, 0),
      intArrayOf(1, 2),
      intArrayOf(2, 0),
      intArrayOf(2, 1),
      intArrayOf(2, 2)
    )
    assertArrayEquals(expected.toTypedArray(), gameOfLife3X3.getNeighboursCords(intArrayOf(1, 1)).toTypedArray())
  }

  @Test
  fun getNeighboursCordsUpperLeftCornerCords() {
    val expected = listOf(
      intArrayOf(2, 2),
      intArrayOf(2, 0),
      intArrayOf(2, 1),
      intArrayOf(0, 2),
      intArrayOf(0, 1),
      intArrayOf(1, 2),
      intArrayOf(1, 0),
      intArrayOf(1, 1)
    )
    val actual = gameOfLife3X3.getNeighboursCords(intArrayOf(0, 0))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsUpperRightCornerCords() {
    val expected = listOf(
      intArrayOf(2, 1),
      intArrayOf(2, 2),
      intArrayOf(2, 0),
      intArrayOf(0, 1),
      intArrayOf(0, 0),
      intArrayOf(1, 1),
      intArrayOf(1, 2),
      intArrayOf(1, 0)
    )
    val actual = gameOfLife3X3.getNeighboursCords(intArrayOf(0, 2))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsLowerLeftCornerCords() {
    val expected = listOf(
      intArrayOf(1, 2),
      intArrayOf(1, 0),
      intArrayOf(1, 1),
      intArrayOf(2, 2),
      intArrayOf(2, 1),
      intArrayOf(0, 2),
      intArrayOf(0, 0),
      intArrayOf(0, 1)
    )
    val actual = gameOfLife3X3.getNeighboursCords(intArrayOf(2, 0))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsLowerRightCornerCords() {
    val expected = listOf(
      intArrayOf(1, 1),
      intArrayOf(1, 2),
      intArrayOf(1, 0),
      intArrayOf(2, 1),
      intArrayOf(2, 0),
      intArrayOf(0, 1),
      intArrayOf(0, 2),
      intArrayOf(0, 0)
    )
    val actual = gameOfLife3X3.getNeighboursCords(intArrayOf(2, 2))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsCenterCordsAnotherSize() {
    val expected = listOf(
      intArrayOf(1, 2),
      intArrayOf(1, 3),
      intArrayOf(1, 4),
      intArrayOf(2, 2),
      intArrayOf(2, 4),
      intArrayOf(3, 2),
      intArrayOf(3, 3),
      intArrayOf(3, 4)
    )
    val actual = gameOfLife5X4.getNeighboursCords(intArrayOf(2, 3))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsUpperLeftCornerCordsAnotherSize() {
    val expected = listOf(
      intArrayOf(3, 4),
      intArrayOf(3, 0),
      intArrayOf(3, 1),
      intArrayOf(0, 4),
      intArrayOf(0, 1),
      intArrayOf(1, 4),
      intArrayOf(1, 0),
      intArrayOf(1, 1)
    )
    val actual = gameOfLife5X4.getNeighboursCords(intArrayOf(0, 0))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsUpperRightCornerCordsAnotherSize() {
    val expected = listOf(
      intArrayOf(3, 3),
      intArrayOf(3, 4),
      intArrayOf(3, 0),
      intArrayOf(0, 3),
      intArrayOf(0, 0),
      intArrayOf(1, 3),
      intArrayOf(1, 4),
      intArrayOf(1, 0)
    )
    val actual = gameOfLife5X4.getNeighboursCords(intArrayOf(0, 4))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsLowerLeftCornerCordsAnotherSize() {
    val expected = listOf(
      intArrayOf(2, 4),
      intArrayOf(2, 0),
      intArrayOf(2, 1),
      intArrayOf(3, 4),
      intArrayOf(3, 1),
      intArrayOf(0, 4),
      intArrayOf(0, 0),
      intArrayOf(0, 1)
    )
    val actual = gameOfLife5X4.getNeighboursCords(intArrayOf(3, 0))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighboursCordsLowerRightCornerCordsAnotherSize() {
    val expected = listOf(
      intArrayOf(2, 3),
      intArrayOf(2, 4),
      intArrayOf(2, 0),
      intArrayOf(3, 3),
      intArrayOf(3, 0),
      intArrayOf(0, 3),
      intArrayOf(0, 4),
      intArrayOf(0, 0)
    )
    val actual = gameOfLife5X4.getNeighboursCords(intArrayOf(3, 4))
    assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
  }

  @Test
  fun getNeighbours() {
    val cordsList = listOf(
      intArrayOf(0, 0),
      intArrayOf(0, 1),
      intArrayOf(0, 2),
      intArrayOf(1, 0),
      intArrayOf(1, 2),
      intArrayOf(2, 0),
      intArrayOf(2, 1),
      intArrayOf(2, 2)
    )
    val expected = listOf(1, 0, 0, 0, 0, 0, 0, 0)
    assertEquals(expected, gameOfLife3X3.getNeighbours(cordsList))
  }

  @Test
  fun getNeighboursAnotherSize() {
    val cordsList = listOf(
      intArrayOf(3, 4),
      intArrayOf(3, 0),
      intArrayOf(3, 1),
      intArrayOf(0, 4),
      intArrayOf(0, 1),
      intArrayOf(1, 4),
      intArrayOf(1, 0),
      intArrayOf(1, 1)
    )
    val expected = listOf(1, 0, 1, 1, 1, 1, 1, 1)
    assertEquals(expected, gameOfLife5X4.getNeighbours(cordsList))
  }
}
