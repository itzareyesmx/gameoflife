@file:JvmName("Main")

package com.gameoflife

import java.io.File

fun main(string: Array<String>) {
  val gameOfLife = GameOfLife(initializeFromFile())

  while (true) {
    gameOfLife.clearConsole()
    gameOfLife.print()
    gameOfLife.transform()
  }
}

private fun initializeFromFile(): Array<IntArray> {
  val path = "src/main/resources/init.txt"
  val lineList = mutableListOf<String>()
  File(path).useLines { lines -> lines.forEach { lineList.add(it) } }

  val matrix = Array(lineList.indices.count()) { IntArray(lineList[0].length) }
  lineList.forEachIndexed { index, element ->
    element.mapIndexed { idx, it -> matrix[index][idx] = it.toString().toInt() }
  }
  return matrix
}
