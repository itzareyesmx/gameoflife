package com.gameoflife

class GameOfLife(
  var grid: Array<IntArray>
) {

  fun transform() {
    val transformedGrid = Array(grid.indices.count()) { IntArray( grid[0].size) }
    for (i in grid.indices) {
      var j = 0
      while (j < grid[i].size) {
        val cordList = getNeighboursCords(intArrayOf(i, j))
        val neighbours = getNeighbours(cordList)
        val totalNeighboursAlive = neighbours.filter { it -> it == 1 }.size
        val newStatus = applyRules(grid[i][j], totalNeighboursAlive)
        transformedGrid[i][j] = newStatus
        j++
      }
    }
    grid =  transformedGrid
  }

  fun getNeighboursCords(cords: IntArray): List<IntArray> {
    val cordList = mutableListOf<IntArray>()
    val limitX = grid.indices.count()
    val limitY = grid[0].size

    for (x in cords[0] - 1..cords[0] + 1) {
      for (y in cords[1] - 1..cords[1] + 1) {
        cordList.add(
          intArrayOf(getNeighbourCords(x, limitX), getNeighbourCords(y, limitY))
        )
      }
    }
    return cordList.filter { item -> !item.contentEquals(cords) }
  }

  fun getNeighbours(cordsList: List<IntArray>): List<Int> {
    val neighbours = mutableListOf<Int>()
    cordsList.forEach {
      neighbours.add(grid[it[0]][it[1]])
    }
    return neighbours
  }

  fun applyRules(actualStatus: Int, totalNeighboursAlive: Int) = if (actualStatus == 1) {
    applyRulesCellAlive(totalNeighboursAlive)
  } else {
    applyRulesCellDead(totalNeighboursAlive)
  }

  fun print(){
    for (i in grid.indices) {
      var j = 0
      while (j < grid[i].size) {
        val value = grid[i][j]
        if(value == 0) print(" ") else print("*")
        j++
      }
      println()
    }
  }

  fun clearConsole(){
    Thread.sleep(100)
    print("\u001b[H\u001b[2J")
  }

  private fun getNeighbourCords(axis: Int, limitAxis: Int) = when (axis) {
    -1 -> axis + limitAxis
    limitAxis -> 0
    else -> axis
  }

  private fun applyRulesCellDead(totalNeighboursAlive: Int) = if (totalNeighboursAlive == 3) 1 else 0

  private fun applyRulesCellAlive(totalNeighboursAlive: Int) = when {
    totalNeighboursAlive < 2 -> 0
    totalNeighboursAlive == 2 -> 1
    totalNeighboursAlive == 3 -> 1
    else -> 0
  }
}
